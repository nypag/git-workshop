# Übung 04: Merging

## Teil 1

Mach Dich mit dem Konzept des Mergings vertraut:

* [Merging in Git](https://git-scm.com/docs/git-merge)

### Hilfsfragen

* Wie wird mit Merge-Konflikten umgegangen?
* Was sind Merge-Strategien?

## Teil 2

1. Wechsle zu Deinem Branch, z.B. `feature/[dein-kuerzel]`
2. Erstelle eine Datei `colors.txt` im Ordner `exercises` mit folgendem
   Inhalt
   ```
   red
   green
   blue
   ```
3. Merge den Branch `feature/tolle-farben` in Deinen Branch
4. OPTIONAL: Versuche den Merge-Konflikt zu lösen
