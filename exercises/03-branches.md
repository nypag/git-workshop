# Übung 03: Branches

## Teil 1

Mach Dich mit dem Konzept der Branches vertraut:

* [Branches in Git](https://git-scm.com/docs/git-branch)
* [Auschecken von Branches](https://git-scm.com/docs/git-checkout)

### Hilfsfragen

* Was macht der Befehl `git branch`?
* Wie können sämtliche Branches aufgelistet werden?
* Wie kann ein Branch geholt/ausgecheckt werden?

## Teil 2

1. Wähle ein Kürzel Deinem Namen entsprechend.
2. Erstelle einen eigenen Branch namens `feature/[dein-kuerzel]`
3. Wechsle zu Deinem Branch `feature/[dein-kuerzel]`
3. Wechsle zurück zum `master` branch
