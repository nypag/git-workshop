# Übung 06: Reverting

## Teil 1

Mach Dich mit dem Konzept von Reverts vertraut:

* [Revert in Git](https://git-scm.com/docs/git-revert)

### Hilfsfragen

* Muss immer ein Commit zum Reverten angegeben werden?
* Kann mehr als ein Commit zum Reverten angegeben werden?
* Wie kann ein Revert abgebrochen, fortgefahren oder übersprungen werden?

## Teil 2

1. Wechsle zu Deinem Branch, z.B. `feature/[dein-kuerzel]`
2. Erstelle einen beliebigen Commit
3. Reverte Deinen zuvor erstellten Commit
