# Übung 05: Rebasing

## Teil 1

Mach Dich mit dem Konzept des Rebasings vertraut:

* [Rebase in Git](https://git-scm.com/docs/git-rebase)

### Hilfsfragen

* Wird ein Rebase auch automatisch vorgenommen? Falls ja, wann?
* Was hat ein Rebase in der Regel zum Ziel?
* Wann ist ein Rebase sinnvoll?
* Wie kann ein Rebase abgebrochen, fortgefahren oder übersprungen werden?

## Teil 2

1. Wechsle zu Deinem Branch, z.B. `feature/[dein-kuerzel]`
2. Erstelle eine neuen Branch, z.B. `bugfix/mein-toller-fix`
3. Erstelle einen beliebigen Commit
4. Wechsle zurück zu Deinem Branch
5. Erstelle einen beliebigen Commit
6. Wechsle wieder zu Deinem neuen Branch, z.B. `bugfix/mein-toller-fix`
7. Rebase Deinen neuen Branch auf Deinen älteren Branch, also z.B. `bugfix/mein-toller-fix` zu `feature/[dein-kuerzel]`
