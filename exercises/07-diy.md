# Übung 07: Do It Yourself (DIY)

## Git Commit Messages

Lies Dir [diesen Artikel](https://chris.beams.io/posts/git-commit/) zu Git
Commit Messages durch.

Erstelle nun mindestens einen Commit basierend auf obigem Artikel.

## Git Log & GitK

Lies Dir folgende Artikel durch:

* [Log in Git](https://git-scm.com/docs/git-log)
* [Gitk](https://git-scm.com/docs/gitk)

Mach Dich mit den beiden Dingen vertraut in dem Du verschiedene
Befehlsargumente ausprobierst. Versuche `gitk` zum Laufen zu bringen und
schau Dir das Interface an.

Erreichst Du mit `gitk` dasselbe wie mit `git log`?

## GitFixUm

Lies Dir [diesen Artikel](https://sethrobertson.github.io/GitFixUm/) zum
Umgang mit ungewollten Commits durch.

Versuche einen Commit auf Bitbucket rückgäng zu machen. Gelingt Dir dies?
Falls nicht, hast Du eine Idee wieso?
