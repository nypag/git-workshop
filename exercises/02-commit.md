# Übung 02: Commit

## Teil 1

Mach Dich mit dem Befehle `commit` von Git vertraut:

* [Commit](https://git-scm.com/docs/git-commit)

### Hilfsfragen

* Wie kann eine Commit-Nachricht angegeben werden?

## Teil 2

Erstelle einen ersten eigenen Commit.
