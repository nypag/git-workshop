# Übung 01: Fetch, Pull und Push

## Teil 1

Mach Dich mit den Befehlen `fetch`, `pull` und `push` von Git vertraut:

* [Fetch](https://git-scm.com/docs/git-fetch)
* [Pull](https://git-scm.com/docs/git-pull)
* [Push](https://git-scm.com/docs/git-push)

### Hilfsfragen

* Was macht `git fetch --all`?
* Wann wird ein `fetch` (automatisch) ausgeführt?
* Was ist der Unterschied zwischen `fetch` und `pull`?

## Teil 2

Führe die Befehle `fetch`, `pull` und `push` alle mindestens einmal aus. Was
stellst Du fest?

Versuche Dich an verschiedenen Argumenten der Befehle, die Du unter oben
stehenden Links findest.
